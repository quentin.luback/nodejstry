const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

(async function() {
  // Connection URL
  const url = 'mongodb://localhost:27017/test';
  // Database Name
  const dbName = 'myproject';
  const client = new MongoClient(url, { useNewUrlParser: true });

  try {
    // Use connect method to connect to the Server
    await client.connect();

    const db = client.db(dbName);
    
    const col = db.collection('dates');

    let r = await db.collection('dates').insertOne({date: new Date()});
    assert.equal(1, r.insertedCount);
    console.log(col);

  } catch (err) {
    console.log(err.stack);
  }

  client.close();
})();
