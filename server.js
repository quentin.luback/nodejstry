const express = require('express')
const bodyParser = require("body-parser")
const fs = require('fs');
const app = express()

app.use(express.json());

app.post('/chat', function (req, res){
  // Exo 1.3
  var obj = JSON.parse(fs.readFileSync('réponses.json', 'utf8'));
  var key = Object.keys(obj);
  var value = req.body.msg;
  if(value.includes(" = ")) {
    var str = value.split(' = ');
    fs.writeFileSync('réponses.json','{"' + str[0] + '": "'+ str[1] + '"}','utf8');
    res.send('Merci pour cette information !');
  } else {
    if(value == key[0]) {
      res.send(key[0] + ': ' + obj[key]);
    }
    else {
      res.send('Je ne connais pas ' + value);
    }
  }
  
  if(req.body.msg === 'ville'){
    res.send('Nous sommes demain');
  }
  else if(req.body.msg === 'demain'){
    res.send('Il fait beau');
  }
});

const port = process.env.PORT || 3000;


app.listen(port, function () {
    console.log('Example app listening on port : ', port)
})
